package com.frontier.inair.arcore

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.frontier.inair.arcore.databinding.FragmentFirstBinding
import com.frontier.inair.arcore.model.AirData
import com.frontier.inair.arcore.model.AirDataViewModel
import com.frontier.inair.arcore.utils.TimeUtils
import com.frontier.inair.arcore.utils.TimeUtils.formatTime
import com.frontier.inair.arcore.utils.log.DEBUG
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var binding: FragmentFirstBinding? = null

    private lateinit var handler: Handler
    private lateinit var updateTimeRunnable: Runnable

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        DEBUG.d("# onCreateView")
        if (binding == null) {
            binding = FragmentFirstBinding.inflate(inflater, container, false)
            binding!!.viewModel = this@FirstFragment

            handler = Handler(Looper.getMainLooper())

            updateTimeRunnable = object : Runnable {
                override fun run() {
                    val currentTimeInSeconds = TimeUtils.getCurrentTimeInSeconds()
                    val formattedTime = TimeUtils.formatTime(currentTimeInSeconds)
//                DEBUG.d("# formattedTime : " + formattedTime)
                    binding!!.tvMainClockWeather.text = formattedTime
                    handler.postDelayed(this, 1000) // 1초마다 업데이트
                }
            }
        }

        // LiveData를 관찰하여 데이터가 변경될 때마다 UI를 업데이트합니다.
        (activity as? MainActivity)?.viewModel!!.airData.observe(viewLifecycleOwner, Observer { airData ->
            // UI를 업데이트하는 코드를 작성합니다.
            DEBUG.d("# updateGifUI fist")
            DEBUG.d("# airData :" + airData.toString())
            updateUI(airData)
            if (airData.iaq != null && airData.iaq.isNotBlank()) {
                val iaqValue = airData.iaq.toDouble().toInt()
                setAirStatusCenterImage(
                    AirFigureTable.getTable(
                        iaqValue
                    )
                )
            }
        })

        return binding!!.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // 여기서 activity를 초기화합니다.
    }

    override fun onResume() {
        super.onResume()
        handler.post(updateTimeRunnable) // Runnable을 실행하여 초마다 업데이트 시작
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(updateTimeRunnable) // 화면이 보이지 않으면 업데이트 중지
    }

    private fun updateUI(airData: AirData) {
        // UI 업데이트 로직을 작성합니다.
        // 예를 들어 TextView 등에 값을 설정하는 등의 작업을 수행합니다.
        binding?.tvMainTemperature!!.text = airData.temperature + "°C"
        binding?.tvMainHumidity!!.text = airData.humidty + "%"
        binding?.tvMainVoc!!.text = airData.voc
        binding?.tvMainIaq!!.text = airData.iaq
    }

    fun onClicked(view: View) {
        when (view.id) {
            R.id.tvMainMenu -> {
                DEBUG.d("# tvMainMenu")
                // 2번째 화면으로 전환
                (activity as? MainActivity)?.fragmentChange(2)
            }
            R.id.tvBtnHelp -> {
                DEBUG.d("# tvBtnHelp")
                (activity as? MainActivity)?.fragmentChange(3)
            }
        }
    }

    /**
     * 중앙 이미지 값 set
     */
    fun setAirStatusCenterImage(value: Int) {
        DEBUG.d("# value : " + value)
        var infoText = "공기질이"
        var drawableResource = when (value) {
            AirFigureTable.EXCELLENT -> R.drawable.large_good
            AirFigureTable.GOOD -> R.drawable.large_normal
            AirFigureTable.SLIGHTLY_BAD -> R.drawable.large_bit_bad
            AirFigureTable.BAD -> R.drawable.large_bad
            AirFigureTable.SERIOUS -> R.drawable.large_serious
            else -> R.drawable.large_very_bad // 예외 처리: 기본 이미지 리소스
        }

        Glide.with(requireContext())
            .load(drawableResource)
            .into(binding!!.ivAirStatus)

        infoText += when (value) {
            AirFigureTable.EXCELLENT -> "좋은"
            AirFigureTable.GOOD -> "보통인"
            AirFigureTable.SLIGHTLY_BAD -> "약간나쁜"
            AirFigureTable.BAD -> "나쁜"
            AirFigureTable.SERIOUS -> "심각한"
            AirFigureTable.VERY_BAD -> "매우 나쁜"
            else -> "" // 예외 처리: 상태가 정의되지 않은 경우
        }

        binding!!.tvAirStatus.text = "$infoText 상태입니다."
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}