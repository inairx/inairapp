package com.frontier.inair.arcore.utils

import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {
    fun getCurrentTimeInSeconds(): Long {
        val date = Date()
        return date.time / 1000
    }

    fun formatTime(seconds: Long): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd a HH:mm", Locale.getDefault())
        return sdf.format(Date(seconds * 1000)) // 초를 밀리초로 변환하여 포맷팅
    }
}