package com.frontier.inair.arcore.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.frontier.inair.arcore.MainActivity;
import com.frontier.inair.arcore.utils.log.DEBUG;

import org.json.JSONException;
import org.json.JSONObject;

public class PermissionActivity extends AppCompatActivity {
    private static final String TAG = PermissionActivity.class.getSimpleName();
    // 권한 승인 코드
    private int RESULT_PERMISSIONS = 100;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        requestPermission();
        Log.e(TAG, "start!!");
    }

    /**
     * Request permission.
     */
    public void requestPermission() {
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (
                cameraPermission == PackageManager.PERMISSION_GRANTED


        ) { // 모두 동의함
            startFirstActivity();
        } else {
            String[] _permissions = new String[]{
                    Manifest.permission.CAMERA
            };

            ActivityCompat.requestPermissions(this, _permissions, RESULT_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        DEBUG.d("onRequestPermissionsResult Act");

        if (grantResults.length == 0) {
            requestPermission();
        } else {
            if (requestCode == RESULT_PERMISSIONS) {
                boolean isGranted = true;

                for (int i = 0; i < grantResults.length; i++) {
                    DEBUG.d("permissions[" + i + "] : " + permissions[i] + ", grantResults[" + i + "] : " + grantResults[i]);
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED || permissions[i].equals(Manifest.permission.POST_NOTIFICATIONS)) { //동의
                    } else { //거부
                        isGranted = false;
                        break;
                    }
                } //for

                if (isGranted) {
                    startFirstActivity();
                } else {
                    Toast.makeText(getApplicationContext(), "[설정] > [권한] 에서 권한을 허용해야 사용할 수 있습니다.", Toast.LENGTH_SHORT).show();
                    finishAppDelay();
                }
            }
        }
    }


    /**
     * Start login activity.
     */
    public void startFirstActivity() {
        DEBUG.d("# startFirstActivity !");
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    /**
     * 1초 후 App 종료
     */
    private void finishAppDelay() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                finishAffinity();
                System.runFinalizersOnExit(true);
                System.exit(0);
            }
        }, 2000);
    }
}
