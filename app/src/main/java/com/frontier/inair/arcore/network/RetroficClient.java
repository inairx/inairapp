package com.frontier.inair.arcore.network;

import com.frontier.inair.arcore.utils.log.DEBUG;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroficClient {

    public static ApiService mService;
    /**
     * Singleton Class
     * 실제 서버 Url 생성 생성자
     * @return - this class
     */
    public static ApiService getInstance() {
        if (mService == null)
            mService = RetroficClient.createService(HttpUrl.parse(NETWORK.DEFAULT_BASE_URL));
        return mService;
    }

    // 단위 테스트용 Url 생성 생성자
    public static ApiService getInstance(HttpUrl url) {
        if (mService == null)
            mService = RetroficClient.createService(url);
        return mService;
    }

    /**
     * ApiService createService
     * @param url host url
     * @param isMultiPart 큰 사이즈 이미지 받는 경우 대기 시간 늘리기 위한 bool 값
     * @return ApiService Interface
     */
    public static ApiService createService(HttpUrl url, boolean isMultiPart) {
        try {
            SSLContext mySSLContext = SSLContext.getInstance("TLS");
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[]{};
                }

                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            }};

            mySSLContext.init(null, trustAllCerts, new SecureRandom());

            HostnameVerifier myHostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

//            OkHttpClient okHttpClient = getUnsafeOkHttpClient(BuildConfig.DEBUG);
            OkHttpClient okHttpClient = getUnsafeOkHttpClient(true, isMultiPart);

            Gson gson = new GsonBuilder().setLenient().create();

            return new Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .build().create(ApiService.class);

        } catch (Exception e) {
            e.printStackTrace();
            DEBUG.d("#error : " + e);
        }
        return null;
    }
    public static ApiService createService(HttpUrl url) {
        return createService(url, false);
    }

    /**
     * SSL 인증서를 신뢰할 수 없을 때 사용
     *
     * @param showLog Retrofit Log 출력@
     * @Param isMultiPart 멀티 파트 업로드 이미지 생성
     * @return OkHttpClient
     */
    private static OkHttpClient getUnsafeOkHttpClient(boolean showLog, boolean isMultiPart) {
        try {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (isMultiPart) {
                builder.connectTimeout(100, TimeUnit.SECONDS);
                builder.writeTimeout(50, TimeUnit.SECONDS);
                builder.readTimeout(50, TimeUnit.SECONDS);
            } else {
                builder.connectTimeout(100, TimeUnit.SECONDS);
                builder.writeTimeout(10, TimeUnit.SECONDS);
                builder.readTimeout(10, TimeUnit.SECONDS);
            }

            OkHttpClient okHttpClient;
            if (showLog) {
                DEBUG.d("# showLog");
                LoggingInterceptor interceptor = new LoggingInterceptor();
                CookieManager cookieManager = new CookieManager();
                cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

                okHttpClient = builder.addInterceptor(interceptor).build();
            } else {
                okHttpClient = builder.build();
            }

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
