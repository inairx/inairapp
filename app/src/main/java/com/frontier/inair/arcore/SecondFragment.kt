package com.frontier.inair.arcore

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.frontier.inair.arcore.databinding.FragmentSecondBinding
import com.frontier.inair.arcore.model.AirDataViewModel
import com.frontier.inair.arcore.utils.TimeUtils
import com.frontier.inair.arcore.utils.log.DEBUG

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    private lateinit var binding: FragmentSecondBinding
    private lateinit var mActivity: MainActivity

    private lateinit var handler: Handler
    private lateinit var updateTimeRunnable: Runnable

    val imageViewArray = arrayOfNulls<ImageView>(4)

    var mGifIndex = 0;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSecondBinding.inflate(inflater, container, false)
        binding.viewModel = this@SecondFragment
        init()
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mActivity = context
        }
    }

    override fun onResume() {
        super.onResume()
        binding.myCameraX.onResume()
        handler.post(updateTimeRunnable) // Runnable을 실행하여 초마다 업데이트 시작
    }

    override fun onPause() {
        super.onPause()
        binding.myCameraX.onPause()
        handler.removeCallbacks(updateTimeRunnable) // 화면이 보이지 않으면 업데이트 중지
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.myCameraX.onDestroyView()
    }

    fun onClicked(view: View) {
        when (view.id) {
            R.id.tvMainMenu -> {
                DEBUG.d("# tvMainMenu")
                // 1번째 화면으로 전환
                mActivity.fragmentChange(1)
            }
        }
    }

    fun init() {
        binding.myCameraX.initCamera(activity)

        handler = Handler(Looper.getMainLooper())
        imageViewArray[0] = binding.ivGif1
        imageViewArray[1] = binding.ivGif2
        imageViewArray[2] = binding.ivGif3
        imageViewArray[3] = binding.ivGif4
        updateTimeRunnable = object : Runnable {
            override fun run() {
                val currentTimeInSeconds = TimeUtils.getCurrentTimeInSeconds()
                val formattedTime = TimeUtils.formatTime(currentTimeInSeconds)
//                DEBUG.d("# formattedTime : " + formattedTime)
                binding.tvTime.text = formattedTime
                handler.postDelayed(this, 1000) // 1초마다 업데이트
            }
        }

        mActivity.viewModel.airData.observe(viewLifecycleOwner, Observer { airData ->
            // UI를 업데이트하는 코드를 작성합니다.
            DEBUG.d("# airData :" + airData.toString())
            updateGifUI()
        })

        updateGifUI()
    }

    fun updateGifUI() {
        DEBUG.d("# updateGifUI")
        mGifIndex = 0;

        if (mActivity.mAirData.iaq.toDouble() < 100) {
            Glide.with(this)
                .asGif()
                .load(R.drawable.iaq)
                .into(imageViewArray[mGifIndex++]!!)
        }
        if (mActivity.mAirData.voc.toDouble() > 25) {
            Glide.with(this)
                .asGif()
                .load(R.drawable.voc)
                .into(imageViewArray[mGifIndex++]!!)
        }
        if (mActivity.mAirData.humidty.toDouble() > 50) {
            Glide.with(this)
                .asGif()
                .load(R.drawable.humidy)
                .into(imageViewArray[mGifIndex++]!!)
        }
        if (mActivity.mAirData.temperature.toDouble() > 50) {
            Glide.with(this)
                .asGif()
                .load(R.drawable.temperature)
                .into(imageViewArray[mGifIndex++]!!)
        }
        binding.tvIaqValue.text = mActivity.mAirData.iaq
        binding.tvVocValue.text = mActivity.mAirData.voc
        binding.tvTemperatureValue.text = mActivity.mAirData.temperature
        binding.tvHumidityValue.text = mActivity.mAirData.humidty
        binding.tvDustValue.text = mActivity.mAirData.dust
    }
}