package com.frontier.inair.arcore

class AirFigureTable {
    // 1.좋음  2.보통  3.약간나쁨  4.나쁨 5.심각 6.매우나쁨
    companion object {
        const val EXCELLENT = 0
        const val GOOD = 1
        const val SLIGHTLY_BAD = 2
        const val BAD = 3
        const val SERIOUS = 4
        const val VERY_BAD = 5

        fun getTable(value: Int): Int {
            return when {
                value >= -50 -> EXCELLENT
                value >= -100 -> GOOD
                value >= -150 -> SLIGHTLY_BAD
                value >= -200 -> BAD
                value >= -300 -> SERIOUS
                else -> VERY_BAD
            }
        }

    }

}