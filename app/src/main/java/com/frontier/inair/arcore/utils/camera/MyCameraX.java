package com.frontier.inair.arcore.utils.camera;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.AspectRatio;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraControl;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.MeteringPoint;
import androidx.camera.core.MeteringPointFactory;
import androidx.camera.core.Preview;
import androidx.camera.core.SurfaceOrientedMeteringPointFactory;
import androidx.camera.core.VideoCapture;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.lifecycle.LifecycleOwner;

import com.frontier.inair.arcore.MainActivity;
import com.frontier.inair.arcore.R;
import com.frontier.inair.arcore.databinding.CommponentCameraPreivewBinding;
import com.frontier.inair.arcore.network.NETWORK;
import com.frontier.inair.arcore.utils.log.DEBUG;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class MyCameraX extends RelativeLayout {
    public CommponentCameraPreivewBinding mBinding;

    private Executor executor = Executors.newSingleThreadExecutor();
    private ImageCapture mImageCapture;
    private CameraControl cameraControl;
    private CameraSelector cameraSelector;
    public ProcessCameraProvider cameraProvider;
    private Camera mCamera;
    VideoCapture videoCapture;
    boolean mIsRecording = false; // 녹화중 상태 값
    private Preview preview;

    private Activity mActivity;

    static public final int RESOLUTION_FHD_WIDTH = 1920;
    static public final int RESOLUTION_FHD_HEIGHT = 1080;

    private int mVideoResolutionWidth = RESOLUTION_FHD_WIDTH;
    private int mVideoResolutionHeight = RESOLUTION_FHD_HEIGHT;;

    // Flash Status Value
    private boolean mFlashStatus = false;
    // 망원 Status Value
    private boolean mIsTelephoto = false;

    // 녹화 시간 표시 관련 객체
    private long recordingStartTime;
    private final long maxRecordingTimeMillis = 60 * 60 * 1000; // 60분을 밀리초로 설정
    // Camera Init
    private boolean isInitialized = false;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    // 녹화 시간 표시 UI
    private Runnable mUpdateChronometer = new Runnable() {
        @Override
        public void run() {
            long elapsedTime = SystemClock.elapsedRealtime() - recordingStartTime;
            if (mBinding == null) return;
            mBinding.chronometer.setBase(SystemClock.elapsedRealtime() - elapsedTime);
            // 녹화 시간 초과 검사
            if (elapsedTime >= maxRecordingTimeMillis) {
                stopRecording();
            } else {
                mHandler.postDelayed(this, 1000); // 1초마다 업데이트
            }
        }
    };

    public MyCameraX(Context context) {
        super(context);
        init(context);
    }

    public MyCameraX(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MyCameraX(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mBinding = CommponentCameraPreivewBinding.inflate(LayoutInflater.from(context), this, true);
        mBinding.setModel(this);
    }

    /**
     * Init
     */
    public void initCamera(Activity activity) {
        this.mActivity = activity;
        mVideoResolutionWidth = RESOLUTION_FHD_WIDTH;
        mVideoResolutionHeight = RESOLUTION_FHD_HEIGHT;

        startCamera();
        initUI();
    }

    public void onResume() {
        DEBUG.d("# CameraFragment onResume ");
        isInitialized = false;
    }

    public void onPause() {
        DEBUG.d("# CameraFragment opPause ");
    }

    public void onDestroyView() {
        DEBUG.d("# CameraFragment onDestroyView");
        stopCamera();
    }

    public void stopCamera() {
        if (mIsRecording) {
            DEBUG.d("# mIsRecording");
            videoCapture.stopRecording();
            // 화면 녹화 종료 시 handler 콜백 제거
            mHandler.removeCallbacks(mUpdateChronometer);
            mIsRecording = false;
        }
        // 카메라 자원 해제
        if (cameraProvider != null) {
            cameraProvider.unbindAll();
            cameraProvider = null;
        }
    }

    /**
     * 카메라 Mode 상태에 따라 UI Setting
     */
    private void initUI() {
        if (true) { // Camera Mode
            mBinding.tvCaptureBtn.setText("촬영");
        } else { // Video Mode
            mBinding.tvCaptureBtn.setText("녹화");
        }
    }

    private void startCamera() {
        try {
            cameraProvider = ProcessCameraProvider.getInstance(getContext()).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 캡처와 미리보기의 종횡비 일치
        Preview.Builder previewBuilder = new Preview.Builder();
        previewBuilder.setTargetAspectRatio(AspectRatio.RATIO_16_9); // 적절한 종횡비로 설정

        preview = previewBuilder.build();
        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
//                .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
                .build();

        preview.setSurfaceProvider(mBinding.previewView.getSurfaceProvider());

        getCameraSupportSize();

        DEBUG.d("# mVideoResolutionWidth : " + mVideoResolutionWidth);
        DEBUG.d("# mVideoResolutionHeight : " + mVideoResolutionHeight);

        videoCapture = new VideoCapture.Builder()
                .setTargetRotation(mActivity.getWindowManager().getDefaultDisplay().getRotation())
                .setTargetResolution(new Size(mVideoResolutionWidth, mVideoResolutionHeight)) // Note - 휴대폰 처리하는 경우 변경 원하는 해상도로 설정
                .build();

        // 이미지 캡처 초기화 및 설정
        mImageCapture = new ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY) // 캡처 모드 설정 (옵션)
                .setTargetRotation(mActivity.getWindowManager().getDefaultDisplay().getRotation())
                .setTargetResolution(new Size(mVideoResolutionWidth, mVideoResolutionHeight)) // Note - 휴대폰 처리하는 경우 변경 원하는 해상도로 설정
                .build();

        // 카메라 미리보기에 연결
        cameraProvider.bindToLifecycle((LifecycleOwner) mActivity, cameraSelector, mImageCapture, videoCapture, preview);

        // CameraControl 가져오기
        mCamera = cameraProvider.bindToLifecycle((LifecycleOwner) mActivity, cameraSelector, preview);
        cameraControl = mCamera.getCameraControl();
    }

    private void startRecording() {
        File outputFile = new File(mActivity.getFilesDir(), "video");
        VideoCapture.OutputFileOptions outputFileOptions = new VideoCapture.OutputFileOptions.Builder(outputFile).build();

        videoCapture.startRecording(outputFileOptions, Executors.newSingleThreadExecutor(), new VideoCapture.OnVideoSavedCallback() {
            @Override
            public void onVideoSaved(@NonNull VideoCapture.OutputFileResults outputFileResults) {
                // 비디오 녹화가 성공적으로 완료될 때 호출됩니다.
                File savedFile = outputFile;
                if (savedFile != null) {
                    if (mIsRecording) { // 촬영중에 다른 화면으로 넘어간 경우 해당 함수 false
                        // savedFile에 녹화된 비디오가 저장됩니다.
                        DEBUG.d("#save !!");
                        mActivity.runOnUiThread(() -> {
                        });
                    }
                }
                DEBUG.d("# save false!!");
                mIsRecording = false;
            }

            @Override
            public void onError(int videoCaptureError, @NonNull String message, @Nullable Throwable cause) {
                // 녹화 중 오류 발생 시 호출됩니다.
                DEBUG.d("#on message : " + message);
                mIsRecording = false;
            }
        });

        mIsRecording = true;
        mBinding.tvCaptureBtn.setText("중지");

        // 화면 녹화 시작 시간 저장
        recordingStartTime = SystemClock.elapsedRealtime();

        // Chronometer 시작
        mBinding.chronometer.setBase(SystemClock.elapsedRealtime());
        mBinding.chronometer.start();

        mBinding.llRecordTime.setVisibility(View.VISIBLE);

        // 녹화 진행 중인지 확인하고 1초마다 Chronometer 업데이트
        mHandler.postDelayed(mUpdateChronometer, 1000);
    }

    /**
     * Click 리스너
     *
     * @param view - 클릭 한 뷰
     */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_capture_btn:
                if (true)   // Camera Mode
                    takeCapture();
                else { // Video Mode
                    if (!mIsRecording)
                        startRecording();
                    else
                        stopRecording();
                }
                break;
        }
    }

    private void stopRecording() {
        DEBUG.d("# stopRecording");
        videoCapture.stopRecording();
        mBinding.tvCaptureBtn.setText("녹화");
        // 화면 녹화 종료 시 handler 콜백 제거
        mHandler.removeCallbacks(mUpdateChronometer);

        mBinding.llRecordTime.setVisibility(View.GONE);
    }

    /**
     * 촬영
     */
    public void takeCapture() {
        DEBUG.d("# takeCapture");
        File file = new File(mActivity.getFilesDir(),"capture");

        ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(file).build();
        mImageCapture.takePicture(outputFileOptions, executor, new ImageCapture.OnImageSavedCallback() {
            @Override
            public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                //todo
            }

            @Override
            public void onError(@NonNull ImageCaptureException error) {
                error.printStackTrace();
            }
        });
    }

    // 수동 초점
    public void performManualFocus(float x, float y) {
        MeteringPointFactory factory = new SurfaceOrientedMeteringPointFactory(
                mBinding.previewView.getWidth(), mBinding.previewView.getHeight(), null);

        MeteringPoint point = factory.createPoint(x, y);
        FocusMeteringAction action = new FocusMeteringAction.Builder(point).build();

        cameraControl.startFocusAndMetering(action).addListener(() -> {
            // Focus action complete callback if needed
        }, executor);
    }

    // 플래쉬 On/Off
    private void toggleFlash() {
        mFlashStatus = !mFlashStatus;
        // 프리뷰 시 상태 플래시 모드 변경
        cameraControl.enableTorch(mFlashStatus);
        // 촬영 시에만 플래시 모드 변경
//        mImageCapture.setFlashMode(mFlashStatus ? ImageCapture.FLASH_MODE_ON : ImageCapture.FLASH_MODE_OFF); // ON, OFF, AUTO 중에서 선택

    }

    /**
     * Zoom vaule Setting
     *
     * @param value zoom ratio
     */
    private void setZoom(float value) {
        mCamera.getCameraControl().setZoomRatio(value); // 초기 줌 레벨 (1.0은 기본값)
    }


    /**
     * Camera Status change ( Capture <-> Video )
     */
    public void changeCameraMode() {
        // 녹화중 변경한 경우 녹화 중지 후 파일 저장 하지 않도록 수행
        if (mIsRecording) {
            DEBUG.d("# isRecording");
            mIsRecording = false;
            stopRecording();
        }
        initUI();
    }

    private void resetCamera() {
        DEBUG.d("# resetCamera");
        if (cameraProvider != null) {
            DEBUG.d("# resetCamera != null");
            cameraProvider.unbindAll();
            startCamera();
        }
    }

    /**
     * 지원하는 해상도 Getter
     */
    private void getCameraSupportSize() {
        CameraManager manager = (CameraManager) mActivity.getSystemService(Context.CAMERA_SERVICE);
        String cameraId = null; // 카메라 ID

        try {
            cameraId = manager.getCameraIdList()[0]; // 카메라 목록 중 첫 번째 카메라 선택
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            Size[] sizes = map.getOutputSizes(SurfaceTexture.class);

            for (Size size : sizes) {
                DEBUG.d("size : " + size.getWidth() + "x" + size.getHeight());
            }

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
}
