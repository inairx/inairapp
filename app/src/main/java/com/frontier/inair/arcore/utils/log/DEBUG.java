package com.frontier.inair.arcore.utils.log;

import android.util.Log;

import com.frontier.inair.arcore.BuildConfig;
import com.frontier.inair.arcore.Global;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Created by TCH on 2021-12-10
 *
 * @Author : bin
 */
public class DEBUG {
    /**
     * Debug Mode Log On/Off
     */
    public static boolean mIsDebug = BuildConfig.DEBUG;
    // File에 로그 저장 여부
    public static boolean mIsFileLog = true;

    private static final String TAG = "LogWrapper";
    private static final int LOG_FILE_SIZE_LIMIT = 1024 * 512;
    private static final int LOG_FILE_MAX_COUNT = 2;
    private static final String LOG_FILE_NAME = "FileLog%g.txt";
    private static final SimpleDateFormat formatter =
            new SimpleDateFormat("MM-dd HH:mm:ss.SSS: ", Locale.getDefault());
    private static final Date date = new Date();
    private static Logger mLogger;
    private static FileHandler fileHandler;

    /**
     * 파일 저장 위한 Log init
     */
    static {
        if (mIsFileLog) {
            try {
                String logFilePath = Global.Companion.getLogFileDir()
                        + File.separator + LOG_FILE_NAME;
                if (!new File(logFilePath).getParentFile().exists())
                    new File(logFilePath).getParentFile().mkdirs();

                fileHandler = new FileHandler(logFilePath, LOG_FILE_SIZE_LIMIT, LOG_FILE_MAX_COUNT, true);
                fileHandler.setFormatter(new Formatter() {
                    @Override
                    public String format(LogRecord logRecord) {
                        date.setTime(System.currentTimeMillis());

                        StringBuilder ret = new StringBuilder(80);
                        ret.append(formatter.format(date));
                        ret.append(logRecord.getMessage());
                        return ret.toString();
                    }
                });

                mLogger = Logger.getLogger(DEBUG.class.getName());
                mLogger.addHandler(fileHandler);
                mLogger.setLevel(Level.ALL);
                mLogger.setUseParentHandlers(false);
                Log.d(TAG, "init success");
            } catch (IOException e) {
                Log.d(TAG, "init failure : " + e.toString());
            }
        }
    }

    /**
     * debug
     *
     * @param _str the str
     * @Comment : Debug Message, 호출한 파일 출력
     */

    public static void d(String _str) {
        if (mIsDebug) {
            Exception e = new Exception();
            StackTraceElement element = e.getStackTrace()[1];
            Log.d("debug", element.getFileName() + "=>" + _str);
        }
    }

    /**
     * debug
     *
     * @param _str the str
     * @Comment : Debug Message, 호출한 파일 출력
     */

    public static void logFile(String _str) {
        if (mIsFileLog) {
            Exception e = new Exception();
            StackTraceElement element = e.getStackTrace()[1];
            if (mLogger != null) {
                mLogger.log(Level.INFO, element.getFileName() + "=>" + _str + "\n");
            }
        }
        error(_str);
    }

    /**
     * error
     *
     * @param _str the str
     * @Comment : Error Message 출력
     */
    public static void error(String _str) {
        if (mIsDebug) {
            Log.e("error", "=========>" + _str);
        }
    }

    /**
     * Adebug.
     *
     * @param _str the str
     */
    public static void adebug(String _str) {
        Log.d("debug", "=========>" + _str);
    }

    /**
     * 긴 Log 출력
     */
    public static void logLargeString(String str) {
        if(str.length() > 3000) {
            d(str.substring(0, 3000));
            logLargeString(str.substring(3000));
        } else {
            d(str);
        }
    }
}