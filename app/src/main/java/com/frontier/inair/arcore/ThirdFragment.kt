package com.frontier.inair.arcore

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.frontier.inair.arcore.databinding.FragmentSecondBinding
import com.frontier.inair.arcore.databinding.FragmentThirdBinding
import com.frontier.inair.arcore.utils.TimeUtils
import com.frontier.inair.arcore.utils.log.DEBUG

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ThirdFragment : Fragment() {

    private lateinit var binding: FragmentThirdBinding
    private lateinit var mActivity: MainActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentThirdBinding.inflate(inflater, container, false)
        binding.viewModel = this@ThirdFragment
        init()
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mActivity = context
        }
    }

    fun onClicked(view: View) {
        when (view.id) {
            R.id.tvMainMenu -> {
                DEBUG.d("# tvMainMenu")
                // 1번째 화면으로 전환
                mActivity.fragmentChange(1)
            }
        }
    }

    fun init() {
    }
}