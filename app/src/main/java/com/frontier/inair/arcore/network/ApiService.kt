package com.frontier.inair.arcore.network

import com.frontier.inair.arcore.model.AirData
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface  ApiService {
    @GET("jongju0920/feeds")
    fun getAirData(@Query("x-aio-key") apiKey: String): Call<ResponseBody>
}