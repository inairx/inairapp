package com.frontier.inair.arcore.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AirDataViewModel : ViewModel() {
    // LiveData를 사용하여 UI에 데이터를 공유합니다.
    val airData = MutableLiveData<AirData>()

    // 데이터를 업데이트하는 메서드
    fun updateAirData(data: AirData) {
        airData.value = data
    }
}