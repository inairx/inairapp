package com.frontier.inair.arcore.model

data class AirData(
    var temperature: String,
    var humidty: String,
    var voc: String,
    var iaq: String,
    var dust: String
)
