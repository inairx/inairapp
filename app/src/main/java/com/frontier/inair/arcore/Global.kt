package com.frontier.inair.arcore

import android.app.Application
import android.content.Context
import android.os.Build

class Global : Application() {

    override fun onCreate() {
        super.onCreate()
        mContext = applicationContext
    }

    companion object {
        lateinit var mContext: Context
            private set

        fun getContext(): Context {
            return mContext
        }

        fun getTempFileDirPath(): String {
            return mContext.getFileStreamPath("temp").path
        }

        fun getLogFileDir(): String {
            return mContext.getFileStreamPath("Log").path
        }
    }
}