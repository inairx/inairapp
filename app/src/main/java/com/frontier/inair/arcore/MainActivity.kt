package com.frontier.inair.arcore

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.frontier.inair.arcore.databinding.ActivityMainBinding
import com.frontier.inair.arcore.model.AirData
import com.frontier.inair.arcore.model.AirDataViewModel
import com.frontier.inair.arcore.network.ApiService
import com.frontier.inair.arcore.network.NETWORK
import com.frontier.inair.arcore.utils.log.DEBUG
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    lateinit var fragmentManager: FragmentManager
    lateinit var transaction: FragmentTransaction

    lateinit var firstFragment: FirstFragment
    lateinit var secondFragment: SecondFragment

    lateinit var viewModel: AirDataViewModel

    var mAirData: AirData = AirData("","","","", "")

    val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY // 로그 레벨 설정 (BASIC, HEADERS, BODY 등)
    }

    val client = OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor) // 로깅 인터셉터 추가
        .build()

    // 여기부터
    val retrofit = Retrofit.Builder()
        .baseUrl(NETWORK.DEFAULT_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    val apiService = retrofit.create(ApiService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fragmentManager = supportFragmentManager
        transaction = fragmentManager.beginTransaction()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
        }

        //status bar와 navigation bar 모두 투명하게 만드는 코드
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        // Fragment를 추가합니다.
        firstFragment = FirstFragment()
        transaction.replace(R.id.fragment_container, firstFragment)
        transaction.commit()
//        apiService = RetroficClient.getInstance()

        mAirData = AirData("", "", "", "", "")

        // ViewModel 초기화
        viewModel = ViewModelProvider(this).get(AirDataViewModel::class.java)

        val scope = CoroutineScope(Dispatchers.Default)

        // Coroutine을 시작합니다.
        scope.launch {
            while (true) {
                getAirData()
                delay(5000) // 10초 대기합니다.
            }
        }
    }

    /**
     * Air Data Getter
     */
    fun getAirData() {
        DEBUG.d("# getAirData")
        val call = apiService.getAirData(NETWORK.API_KEY)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val rawJson = response.body()?.string()
                    val jsonArray = JSONArray(rawJson)
                    for (i in 0 until jsonArray.length()) {
                        val jsonObject = jsonArray.getJSONObject(i)
                        val username = jsonObject.getString("username")
                        val id = jsonObject.getInt("id")
                        val name = jsonObject.getString("name")
                        val lastValue = jsonObject.optString("last_value", "N/A")
                        val key = jsonObject.optString("key", "N/A")

                        when (key) {
                            "temperature" -> {
                                mAirData.temperature =
                                    String.format("%.1f", lastValue.toDouble())
                            }
                            "humidity" -> {
                                DEBUG.d("# humidity : " + lastValue)
                                mAirData.humidty = lastValue
                            }
                            "iaq" -> {
                                mAirData.iaq = String.format("%.2f", lastValue.toDouble())
                            }
                            "voc" -> {
                                mAirData.voc = lastValue.toDouble().toInt().toString()

                            }
                            "dust" -> {
                                mAirData.dust = lastValue.toDouble().toInt().toString()

                            }
                        }
                    }
                    DEBUG.d("# updateAirData")
                    viewModel.updateAirData(mAirData)
                } else {
                    // 오류 응답을 처리합니다.
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                // 네트워크 오류 처리
                DEBUG.d("# onFailure : " + t.toString())
            }
        })
    }

    fun fragmentChange(index : Int) {
        DEBUG.d("# fragmentChange : " + index)
        when (index) {
            1 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, firstFragment)
                    .commit()

            }
            2 -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, SecondFragment())
                    .commit()
            }
            3  -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, ThirdFragment())
                    .commit()
            }
        }
    }

    //뒤로가기 연속 클릭 대기 시간
    var mBackWait:Long = 0

    override fun onBackPressed() {
//        super.onBackPressed()
        // 뒤로가기 버튼 클릭
        if (System.currentTimeMillis() - mBackWait >= 2000) {
            mBackWait = System.currentTimeMillis()
            Snackbar.make(binding.root, "뒤로가기 버튼을 한번 더 누르면 종료됩니다.", Snackbar.LENGTH_LONG).show()
        } else {
            finish() //액티비티 종료
        }
    }
}