package com.frontier.inair.arcore.network;

import com.frontier.inair.arcore.utils.log.DEBUG;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Http Log 인터셉터
 */
public class LoggingInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Response response = chain.proceed(request);

        MediaType contentType = null;
        String bodyString = null;
        if (response.body() != null) {
            contentType = response.body().contentType();
            bodyString = response.body().string();
        }

        DEBUG.logFile("url : " + request.url());
        DEBUG.d("url : " + request.url());

        // 멀티파트 파트 확인
        if (request.body() instanceof MultipartBody) {
            MultipartBody multipartBody = (MultipartBody) request.body();
            for (MultipartBody.Part part : multipartBody.parts()) {
                // 파일 이름 또는 다른 필요한 정보 로그로 출력
                if (part.headers() != null) {
                    String contentDisposition = part.headers().get("Content-Disposition");
                    if (contentDisposition != null && contentDisposition.contains("filename=")) {
                        String[] fileNameSplit = contentDisposition.split("filename=");
                        if (fileNameSplit.length > 1) {
                            String fileName = fileNameSplit[1].replaceAll("\"", "");
                            DEBUG.logFile("Multipart Part File Name: " + fileName);
                        }
                    }
                }
            }
        } else {
            DEBUG.logFile("request body : " + stringifyRequestBody(request));
        }
        DEBUG.logFile("response body : " + stringifyResponseBody(bodyString));

        if (response.body() != null) {
            ResponseBody body = ResponseBody.create(contentType, bodyString);
            return response.newBuilder().body(body).build();
        } else {
            return response;
        }
    }


    private static String stringifyRequestBody(Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    public String stringifyResponseBody(String responseBody) {
        return responseBody;
    }
}